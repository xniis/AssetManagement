﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    /// <summary>
    /// 低值品与耗材
    /// </summary>
    public class MaterialEntity
    {
        /// <summary>
        /// 低值品编号
        /// </summary>
        public string Id
        {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Amount
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Price
        {
            get; set;
        }

        public string StorageTime
        {
            get; set;
        }

        /// <summary>
        /// 实验室器材的状态
        /// </summary>
        public string State
        {
            get; set;
        }
    }
}
