﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    /*
    public enum USER_TYPE
    {
        Student,
        Teacher,
        Admin
    };*/

    public class AccountEntity
    {
        /*
        private const string _userTypeStudentStr = "student";
        private const string _userTypeAdminStr = "administrator";
        private const string _userTypeTeacherStr = "teacher";

        static public USER_TYPE StringToUserTypeEnum(String str)
        {
            USER_TYPE t;
            switch(str)
            {
                default:
                case _userTypeStudentStr:
                    t = USER_TYPE.Student;
                    break;
                case _userTypeAdminStr:
                    t = USER_TYPE.Admin;
                    break;
                case _userTypeTeacherStr:
                    t = USER_TYPE.Teacher;
                    break;
            }
            return t;
        }

        static public string UserTypeEnumToString(USER_TYPE t)
        {
            string str;
            switch (t)
            {
                default:
                case USER_TYPE.Student:
                    str = _userTypeStudentStr;
                    break;
                case USER_TYPE.Teacher:
                    str = _userTypeTeacherStr;
                    break;
                case USER_TYPE.Admin:
                    str = _userTypeAdminStr;
                    break;        
            }
            return str;
        }*/

        

        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public int UserLevel { get; set; }
    }
}
