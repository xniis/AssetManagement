﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EntityLayer
{
    public class NotificationEntity
    {
        public int Id
        {
            get; set;
        }
        public string Subject
        {
            get; set;
        }
        public DateTime Time
        {
            get; set;
        }

    }
}
