﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer
{
    /// <summary>
    /// 仪器设备
    /// </summary>
    public class DeviceEntity
    {
        /// <summary>
        /// 仪器编号
        /// </summary>
        public string Id
        {
            get; set;
        }

        /// <summary>
        /// 仪器名称
        /// </summary>
        public string Name
        {
            get; set;
        }
        
        /// <summary>
        /// 仪器数量
        /// </summary>
        public int Amount
        {
            get; set;
        }
        
        /// <summary>
        /// 仪器所在实验室编号
        /// </summary>
        public string LabId
        {
            get; set;
        }
        
        /// <summary>
        /// 仪器状态
        /// </summary>
        public string State
        {
            get; set;
        }
    }
}
