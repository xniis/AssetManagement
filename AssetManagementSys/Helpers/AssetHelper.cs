﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetManagementSys.View.Alerts;
using AssetManagementSys.ViewModel;
namespace AssetManagementSys
{
    class AssetHelper
    {
        #region MenuTitle
        public static string AccountManagementName = "用户管理";
        public static string DevicesName = "仪器设备管理";
        public static string MaterialName = "低值品与耗材管理";
        public static string NotificationBoardName = "公告管理";
        public static string AddProduct = "Add Product";
        public static string ModifyProduct = "Modify Products";
        public static string StockEntry = "Stock Entry";
        public static string SellProducts = "Sell Products";
        public static string Transactions = "Transactions";
        #endregion

        private static GrowlNotifiactions _growlNotifications = null; // Glow Notification Object

        public static GrowlNotifiactions GrowlNotification
        {
            get
            {
                if(_growlNotifications == null)
                {
                    _growlNotifications = new GrowlNotifiactions();
                }
                return _growlNotifications;
            }
        }
        public static void SimpleAlert(string _Title, string _Message)
        {
            GrowlNotification.AddNotification(new Notification { Title = _Title, Message = _Message });

        }

        public static void SuccessAlert(string _Title, string _Message)
        {
            GrowlNotification.AddNotification(new Notification { Title = _Title, Message = _Message });

        }

        public static string GetSaveFilePath()
        {
            string DirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string FolderName = "InventoryFiles";
            DirectoryPath += @"\" + FolderName;
            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }
            return DirectoryPath;
        }

        public enum TransactionType
        {
            Credit = 1,
            Debit = 2
        }



        public static DateTime GetFirstDateOfWeek(DateTime dayInWeek, DayOfWeek RunningDay)  //RunningDay is Schedular Running day 
        {
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != RunningDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }
        public static DateTime GetLastDateOfWeek(DateTime dayInWeek, DayOfWeek RunningDay)
        {
            DateTime lastDayInWeek = dayInWeek.Date;
            while (lastDayInWeek.DayOfWeek != RunningDay)
                lastDayInWeek = lastDayInWeek.AddDays(1);

            return lastDayInWeek;
        }

        public static DateTime GetLastDayOfMonth()
        {
            DateTime firstOfNextMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1);
            DateTime lastOfThisMonth = firstOfNextMonth.AddDays(-1);
            return lastOfThisMonth;
        }

        public static DateTime GetLastWeekdayOfMonth(DateTime date, DayOfWeek day)         // Get last WeekdayofMonth according to day
        {
            DateTime lastDayOfMonth = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1);
            int wantedDay = (int)day;
            int lastDay = (int)lastDayOfMonth.DayOfWeek;
            return lastDayOfMonth.AddDays(lastDay >= wantedDay ? wantedDay - lastDay : wantedDay - lastDay - 7);
        }

        public static DateTime GetMonthFirstday(DateTime date)
        {
            DateTime FirstDay = new DateTime(date.Year, date.Month, 1);
            return FirstDay;
        }

        public static DateTime GetMonthLastday(DateTime date)
        {
            DateTime firstDayOfTheMonth = new DateTime(date.Year, date.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public static bool CheckWeekEndDay()
        {
            DateTime currentdate = DateTime.Now.Date;
            DateTime lastDayInWeek = GetLastDateOfWeek(DateTime.Now, DayOfWeek.Sunday);
            if (currentdate == lastDayInWeek)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public static bool CheckMonthWeekEndDay()
        {
            DateTime currentdate = DateTime.Now.Date;
            DateTime monthlastweekend = GetLastWeekdayOfMonth(DateTime.Now, DayOfWeek.Sunday);
            if (currentdate == monthlastweekend)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
