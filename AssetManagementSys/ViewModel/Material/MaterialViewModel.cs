﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using EntityLayer;
using System.Windows.Input;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows;

namespace AssetManagementSys.ViewModel
{
    public class MaterialViewModel : MaterialVMBase
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();

        private IList<MaterialEntity> _tempLoadMaterials;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _tempLoadMaterials = MaterialServices.GetAllMaterials();
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Materials = _tempLoadMaterials;
        }

        private IList<MaterialEntity> _materials;
        public IList<MaterialEntity> Materials
        {
            get
            {
                return _materials;
            }
            set
            {
                _materials = value;
                RaisedPropertyChanged("Materials");
            }
        }

        public MaterialViewModel()
        {
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
            // BindGrid();
        }

        void BindGrid()
        {
            Materials = MaterialServices.GetAllMaterials();
        }

        #region Button Command


        private ICommand _addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(new Action<object>(AddNewItems));
                }
                return _addCommand;
            }
            set
            {
                _addCommand = value;
                RaisedPropertyChanged("AddCommand");

            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(new Action<object>(DeleteSelectItems));
                }
                return _deleteCommand;
            }
            set
            {
                _deleteCommand = value;
                RaisedPropertyChanged("DeleteCommand");

            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(new Action<object>(SaveItems));
                }
                return _saveCommand;
            }
            set
            {
                _saveCommand = value;
                RaisedPropertyChanged("SaveCommand");

            }
        }
        #endregion

        public List<MaterialEntity> _newAddItems = new List<MaterialEntity>();

        public void OnGridRowEditEnding(object senfer, DataGridRowEditEndingEventArgs e)
        {
            var info = e.Row.Item as MaterialEntity;
            if (_editState == EditState.Add)
            {
                _newAddItems.Add(info);
            }
            else if (_editState == EditState.Edit)
            {
                MaterialServices.UpdateMaterial(info);
            }
        }

        List<string> _selectedTagsToDel = new List<string>();

        public void OnCheckBoxClick(object s, RoutedEventArgs e)
        {
            CheckBox dg = s as CheckBox;
            string id = dg.Tag.ToString();
            var c = dg.IsChecked;
            if (c == true)
            {
                _selectedTagsToDel.Add(id);
            }
            else
            {
                _selectedTagsToDel.Remove(id);
            }
        }

        public enum EditState
        {
            Edit,
            Add
        }

        EditState _editState = EditState.Edit;

        public void ChangeEditState(EditState state)
        {
            _editState = state;
        }

        public void AddNewItems(object parameter)
        {
            _editState = EditState.Add;

        }

        public void DeleteSelectItems(object parameter)
        {
            foreach (var id in _selectedTagsToDel)
            {
                var item = Materials.Where(s => s.Id == id)
                           .FirstOrDefault();
                if (item != default(MaterialEntity))
                {
                    MaterialServices.DeleteById(id);

                    // AssetHelper.SuccessAlert("成功", "已删除");
                }
                else
                {
                    AssetHelper.SimpleAlert("错误", String.Format("删除失败，无此材料, id:{0}", id));
                }
            }
            BindGrid();
            AssetHelper.SuccessAlert("成功", "所选项已全部删除");
            _selectedTagsToDel.Clear();
        }

        public void SaveItems(object parameter)
        {
            var grid = parameter as DataGrid;
            foreach(var item in _newAddItems)
            {
                var sameName = MaterialServices.GetMaterialByID(item.Id);
                if(sameName == default(MaterialEntity))
                {
                    MaterialServices.Add(item);
                }
                else
                {
                    AssetHelper.SimpleAlert("错误", String.Format("添加失败，设备id冲突, id:{0}", item.Id));
                }
            }     
            grid.CanUserAddRows = false;
            BindGrid();
            AssetHelper.SuccessAlert("成功", "已保存");
        }
    }
}
