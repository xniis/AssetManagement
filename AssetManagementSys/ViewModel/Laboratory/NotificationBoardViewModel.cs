﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;
using BusinessLayer;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;

namespace AssetManagementSys.ViewModel
{
    public class NotificationBoardViewModel : LaboratoryVMBase
    {
        public override string Name
        {
            get { return AssetHelper.AccountManagementName; }
        }
        private readonly BackgroundWorker worker = new BackgroundWorker();

        private IList<NotificationEntity> _tempLoadNotifications;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _tempLoadNotifications = NotificationServices.GetAllNotifications();
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Notifications = _tempLoadNotifications;
        }

        private IList<NotificationEntity> _Notifications;
        public IList<NotificationEntity> Notifications
        {
            get
            {
                return _Notifications;
            }
            set
            {
                _Notifications = value;
                RaisedPropertyChanged("Notifications");
            }
        }

        public NotificationBoardViewModel()
        {
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
            // BindGrid();
        }

        void BindGrid()
        {
            Notifications = NotificationServices.GetAllNotifications();
        }

        #region Button Command


        private ICommand _addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(new Action<object>(AddNewItems));
                }
                return _addCommand;
            }
            set
            {
                _addCommand = value;
                RaisedPropertyChanged("AddCommand");

            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(new Action<object>(DeleteSelectItems));
                }
                return _deleteCommand;
            }
            set
            {
                _deleteCommand = value;
                RaisedPropertyChanged("DeleteCommand");

            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(new Action<object>(SaveItems));
                }
                return _saveCommand;
            }
            set
            {
                _saveCommand = value;
                RaisedPropertyChanged("SaveCommand");

            }
        }
        #endregion

        public List<NotificationEntity> _newAddItems = new List<NotificationEntity>();

        public void OnGridRowEditEnding(object senfer, DataGridRowEditEndingEventArgs e)
        {
            var info = e.Row.Item as NotificationEntity;
            if (_editState == EditState.Add)
            {
                _newAddItems.Add(info);
            }
            else if (_editState == EditState.Edit)
            {
                NotificationServices.UpdateNotification(info);
            }
        }

        List<int> _selectedTagsToDel = new List<int>();

        public void OnCheckBoxClick(object s, RoutedEventArgs e)
        {
            CheckBox dg = s as CheckBox;
            int id = int.Parse(dg.Tag.ToString());
            var c = dg.IsChecked;
            if (c == true)
            {
                _selectedTagsToDel.Add(id);
            }
            else
            {
                _selectedTagsToDel.Remove(id);
            }
        }

        public enum EditState
        {
            Edit,
            Add
        }

        EditState _editState = EditState.Edit;

        public void ChangeEditState(EditState state)
        {
            _editState = state;
        }

        public void AddNewItems(object parameter)
        {
            _editState = EditState.Add;

        }

        public void DeleteSelectItems(object parameter)
        {
            foreach (var id in _selectedTagsToDel)
            {
                var item = Notifications.Where(s => s.Id == id)
                           .FirstOrDefault();
                if (item != default(NotificationEntity))
                {
                    NotificationServices.DeleteById(id);

                    // AssetHelper.SuccessAlert("成功", "已删除");
                }
                else
                {
                    AssetHelper.SimpleAlert("错误", String.Format("删除失败，无此材料, id:{0}", id));
                }
            }
            BindGrid();
            AssetHelper.SuccessAlert("成功", "所选项已全部删除");
            _selectedTagsToDel.Clear();
        }

        public void SaveItems(object parameter)
        {
            var grid = parameter as DataGrid;
            foreach (var item in _newAddItems)
            {
                NotificationServices.Add(item);

            }
            grid.CanUserAddRows = false;
            BindGrid();
            AssetHelper.SuccessAlert("成功", "已保存");
        }
    }
}
