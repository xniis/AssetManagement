﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using EntityLayer;
using System.Windows.Input;
using System.Windows.Controls;
using System.ComponentModel;

namespace AssetManagementSys.ViewModel
{
    class AccountManagementVM : LaboratoryVMBase
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();

        private IList<AccountEntity> _tempLoadAccounts;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _tempLoadAccounts = AccountServices.GetAllAccounts();
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Accounts = _tempLoadAccounts;
        }

        private string _selectedUserName;
        private string _selectedUserPassword;
        private string _selectedUserID;
        private int _selectedUserLevel;

        public string SelectedUserName
        {
            get
            {
                return _selectedUserName;
            }
            set
            {
                _selectedUserName = value;
                RaisedPropertyChanged("SelectedUserName");

            }
        }

        public string SelectedUserPassword
        {
            get
            {
                return _selectedUserPassword;
            }
            set
            {
                _selectedUserPassword = value;
                RaisedPropertyChanged("SelectedUserPassword");
            }
        }

        public string SelectedUserID
        {
            get
            {
                return _selectedUserID;
            }
            set
            {
                _selectedUserID = value;
                RaisedPropertyChanged("SelectedUserID");
            }
        }

        public int SelectedUserLevel
        {
            get
            {
                return _selectedUserLevel;
            }
            set
            {
                _selectedUserLevel = value;
                RaisedPropertyChanged("SelectedUserLevel");
            }
        }

        private IList<AccountEntity> _accouts;
        public IList<AccountEntity> Accounts
        {
            get
            {
                return _accouts;
            }
            set
            {
                _accouts = value;
                RaisedPropertyChanged("Accounts");
            }
        }

        public AccountManagementVM()
        {
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
           // BindGrid();
        }

        public override string Name
        {
            get { return AssetHelper.AccountManagementName; }
        }

        private void BindGrid()
        {
            Accounts = AccountServices.GetAllAccounts();
        }

        #region Button Command


        private ICommand _addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(new Action<object>(AddUser));
                }
                return _addCommand;
            }
            set
            {
                _addCommand = value;
                RaisedPropertyChanged("AddCommand");

            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(new Action<object>(DeleteUser));
                }
                return _deleteCommand;
            }
            set
            {
                _deleteCommand = value;
                RaisedPropertyChanged("DeleteCommand");
              
            }
        }

        private ICommand _updateCommand;
        public ICommand UpdateCommand
        {
            get
            {
                if (_updateCommand == null)
                {
                    _updateCommand = new RelayCommand(new Action<object>(UpdateUser));
                }
                return _updateCommand;
            }
            set
            {
                _updateCommand = value;
                RaisedPropertyChanged("UpdateCommand");

            }
        }
        #endregion
        //private int _judgeGridState = 0;//0表示编辑状态，1为添加状态。因为后面的增加和编辑都在同一个事件中，所以建一个变量来区分操作  

        public void AddUser(object parameter)
        {
            var user = Accounts.Where(s => s.UserId == SelectedUserID).FirstOrDefault();
            
            if (user == default(AccountEntity)) // new user
            {
                AccountEntity ae = new AccountEntity
                {
                    UserName = SelectedUserName,
                    UserPassword = SelectedUserPassword,
                    UserId = SelectedUserID,
                    UserLevel = SelectedUserLevel
                };
                AccountServices.AddUpdateAccount(ae);
                BindGrid();
                RaisedPropertyChanged("Accounts");
                AssetHelper.SuccessAlert("成功", "已添加新用户");
            }
            else
            {
                AssetHelper.SimpleAlert("错误", "用户名已存在！创建失败");
            }
        }

        public void DeleteUser(object parameter)
        {
            var user = Accounts.Where(s => s.UserId == SelectedUserID).FirstOrDefault();
            if(user != null)
            {
                AccountServices.DelUpdateAccount(user);
                BindGrid();
                //  RaisedPropertyChanged("Accounts");
                AssetHelper.SuccessAlert("成功", "用户已删除");
            }
            else
            {
                AssetHelper.SimpleAlert("错误", "无此用户");
            }
        }


        /// <summary>
        /// 判断有没有该用户名，若有则更新密码等数据
        /// </summary>
        /// <param name="parameter"></param>
        
        public void UpdateUser(object parameter)
        {
            var user = Accounts.Where(s => s.UserId == SelectedUserID).FirstOrDefault();

            if (user != default(AccountEntity))
            {
                AccountEntity a = new AccountEntity
                {
                    UserName = SelectedUserName,
                    UserId = SelectedUserID,
                    UserLevel = SelectedUserLevel,
                    UserPassword = SelectedUserPassword
                };
                AccountServices.UpdateAccount(a);
                BindGrid();
                RaisedPropertyChanged("Accounts");
                AssetHelper.SuccessAlert("成功", "用户已更新");
            }
            else
            {
                AssetHelper.SimpleAlert("错误", "无此用户");
            }
        }
    }
}
