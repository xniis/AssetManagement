﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;
using AssetManagementSys.View;

namespace AssetManagementSys.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();
        
        /*
        private ObservableCollection<DevicesVMBase> _devicesMenu;
        private ObservableCollection<LaboratoryVMBase> _laboratoryMenu;
        private ObservableCollection<MaterialVMBase> _materialMenu;
        */
        
        private ObservableCollection<MenuWrapper> _devicesMenu;
        private ObservableCollection<MenuWrapper> _laboratoryMenu;
        private ObservableCollection<MenuWrapper> _materialMenu;
            
        /// <summary>
        /// 仪器设备管理
        /// </summary>
        public ObservableCollection<MenuWrapper> DevicesMenu
        {
            get
            {
                return _devicesMenu;
            }
            set
            {
                _devicesMenu = value;
                RaisedPropertyChanged("DevicesMenu");
            }
        }

        /// <summary>
        /// 实验室管理
        /// </summary>
        public ObservableCollection<MenuWrapper> LaboratoryMenu
        {
            get
            {
                return _laboratoryMenu;
            }
            set
            {
                _laboratoryMenu = value;
                RaisedPropertyChanged("LaboratoryMenu");
            }
        }

        /// <summary>
        /// 低值品与耗材管理
        /// </summary>
        public ObservableCollection<MenuWrapper> MaterialMenu
        {
            get
            {
                return _materialMenu;
            }
            set
            {
                _materialMenu = value;
                RaisedPropertyChanged("MaterialMenu");
            }
        }

        public MainWindowViewModel()
        {
            worker.DoWork += WorkerDoWork;
            worker.RunWorkerCompleted += WorkerRunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        //private ObservableCollection<DevicesVMBase> _tempDevicesMenu = new ObservableCollection<DevicesVMBase>();
        //private ObservableCollection<LaboratoryVMBase> _tempLaboratoryMenu = new ObservableCollection<LaboratoryVMBase>();
        //private ObservableCollection<MaterialVMBase> _tempMaterialMenu = new ObservableCollection<MaterialVMBase>();
        private ObservableCollection<MenuWrapper> _tempDevicesMenu = new ObservableCollection<MenuWrapper>();
        private ObservableCollection<MenuWrapper> _tempLaboratoryMenu = new ObservableCollection<MenuWrapper>();
        private ObservableCollection<MenuWrapper> _tempMaterialMenu = new ObservableCollection<MenuWrapper>();


        private void WorkerDoWork(object sender, DoWorkEventArgs e)
        {
            #region Devices Menu
            //_tempDevicesMenu.Add(new AccountManagementVM());
            _tempDevicesMenu.Add(new MenuWrapper(typeof(Devices), AssetHelper.DevicesName));
            #endregion

            /////////////////////////////////////////////////////////////////////
            #region Laboratory Menu

            _tempLaboratoryMenu.Add(new MenuWrapper(typeof(AccountManagement), AssetHelper.AccountManagementName));
            _tempLaboratoryMenu.Add(new MenuWrapper(typeof(NotificationBoard), AssetHelper.NotificationBoardName));

            #endregion
            //////////////////////////////////////////////////////////////////////
            #region Material Menu
            _tempMaterialMenu.Add(new MenuWrapper(typeof(Material), AssetHelper.MaterialName));
            #endregion
        }

        private void WorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DevicesMenu = _tempDevicesMenu;
            LaboratoryMenu = _tempLaboratoryMenu;
            MaterialMenu = _tempMaterialMenu;
        }
    }
}
