﻿using System;
using System.Collections.Generic;
using BusinessLayer;
using EntityLayer;
using System.Windows.Input;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows;
using System.Linq;

namespace AssetManagementSys.ViewModel
{
    public class DevicesViewModel : DevicesVMBase
    {

        private readonly BackgroundWorker worker = new BackgroundWorker();

        private IList<DeviceEntity> _tempLoadDevices;

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _tempLoadDevices = DeviceServices.GetAllDevices();
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Devices = _tempLoadDevices;
        }

        private IList<DeviceEntity> _devices;
        public IList<DeviceEntity> Devices
        {
            get
            {
                return _devices;
            }
            set
            {
                _devices = value;
                RaisedPropertyChanged("Devices");
            }
        }

        public DevicesViewModel()
        {
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
            // BindGrid();
        }

        void BindGrid()
        {
            Devices = DeviceServices.GetAllDevices();
        }

        #region Button Command


        private ICommand _addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(new Action<object>(AddNewItems));
                }
                return _addCommand;
            }
            set
            {
                _addCommand = value;
                RaisedPropertyChanged("AddCommand");

            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(new Action<object>(DeleteSelectItems));
                }
                return _deleteCommand;
            }
            set
            {
                _deleteCommand = value;
                RaisedPropertyChanged("DeleteCommand");

            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(new Action<object>(SaveItems));
                }
                return _saveCommand;
            }
            set
            {
                _saveCommand = value;
                RaisedPropertyChanged("SaveCommand");

            }
        }
        #endregion

        public List<DeviceEntity> _newAddItems = new List<DeviceEntity>();

        public void OnGridRowEditEnding(object senfer, DataGridRowEditEndingEventArgs e)
        {
            var info = e.Row.Item as DeviceEntity;
            if (_editState == EditState.Add)
            {
                _newAddItems.Add(info);
            }
            else if (_editState == EditState.Edit)
            {
                DeviceServices.UpdateDevice(info);
            }
        }

        List<string> _selectedTagsToDel = new List<string>();

        public void OnCheckBoxClick(object s, RoutedEventArgs e)
        {
            CheckBox dg = s as CheckBox;
            string id = dg.Tag.ToString();
            var c = dg.IsChecked;
            if(c == true)
            {
                _selectedTagsToDel.Add(id);
            }
            else
            {
                _selectedTagsToDel.Remove(id);
            }
        }

        public enum EditState
        {
            Edit,
            Add
        }

        EditState _editState = EditState.Edit;
        
        public void ChangeEditState(EditState state)
        {
            _editState = state;
        }

        public void AddNewItems(object parameter)
        {
            _editState = EditState.Add;

        }

        public void DeleteSelectItems(object parameter)
        {
            foreach (var id in _selectedTagsToDel)
            {
                var item = Devices.Where(s => s.Id == id)
                           .FirstOrDefault();
                if (item != default(DeviceEntity))
                {
                    DeviceServices.DeleteById(id);
                    
                   // AssetHelper.SuccessAlert("成功", "已删除");
                }
                else
                {
                    AssetHelper.SimpleAlert("错误", String.Format("删除失败，无此设备,设备号:{0}", id));
                }
            }
            BindGrid();
            AssetHelper.SuccessAlert("成功", "所选项已全部删除");
            _selectedTagsToDel.Clear();
        }

        public void SaveItems(object parameter)
        {
            var grid = parameter as DataGrid;
            DeviceServices.AddList(_newAddItems);
            grid.CanUserAddRows = false;
            BindGrid();
            AssetHelper.SuccessAlert("成功", "已保存");
        }


    }
}
