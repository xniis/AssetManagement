﻿using System.Collections.ObjectModel;

namespace AssetManagementSys.ViewModel
{
    public class Notifications : ObservableCollection<Notification> { }

    public class Notification : ViewModelBase
    {
        private string message;
        public string Message
        {
            get { return message; }

            set
            {
                if (message == value) return;
                message = value;
                RaisedPropertyChanged("Message");
            }
        }

        private int id;
        public int Id
        {
            get { return id; }

            set
            {
                if (id == value) return;
                id = value;
                RaisedPropertyChanged("Id");
            }
        }

        private string title;
        public string Title
        {
            get { return title; }

            set
            {
                if (title == value) return;
                title = value;
                RaisedPropertyChanged("Title");
            }
        }




    }

    
}
