﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AssetManagementSys.View
{
    public class WindowViewBase : UserControl
    {
        public string ViewName { get; }
    }
}
