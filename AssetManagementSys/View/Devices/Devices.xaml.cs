﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AssetManagementSys.ViewModel;
namespace AssetManagementSys.View
{
    /// <summary>
    /// Devices.xaml 的交互逻辑
    /// </summary>
    public partial class Devices : UserControl
    {
        private DevicesViewModel vm = null;
        public Devices()
        {
            InitializeComponent();
            vm = new DevicesViewModel();
            this.DataContext = vm;
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            vm.ChangeEditState(DevicesViewModel.EditState.Add);
            dataGrid.CanUserAddRows = true;
        }

        private void dataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            vm.OnGridRowEditEnding(sender, e);
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            vm.OnCheckBoxClick(sender, e);
        }
    }
}
