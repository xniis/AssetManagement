﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AssetManagementSys.View
{
    public class MenuWrapper
    {
        private UserControl _control = null;

        public UserControl Control
        {
            get
            {
                if(_control == null)
                {
                    _control = (UserControl)Activator.CreateInstance(_controlType);
                }
                return _control;
            }
        }

        public string MenuName
        {
            get; set;
        }

        private Type _controlType;

        public MenuWrapper(Type t, string name)
        {
            _controlType = t;
            MenuName = name;
        }
    }
}
