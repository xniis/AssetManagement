﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using AssetManagementSys.ViewModel;

namespace AssetManagementSys.View
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowViewModel();

            ListBoxLab.SelectedIndex = -1;
        }
        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void CommandBinding_CanExecute_1(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void MiniMize_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        public void Drag_Window(object sender, MouseButtonEventArgs e)
        {

            this.DragMove();

        }

        private void ListBoxLab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //pnlcontent.Content = "{Binding ElementName=ListBoxDelears, Path=SelectedItem}";
            if (ListBoxLab.SelectedIndex != -1)
            {
                PanelContent.Children.Clear();
                PanelContent.Children.Add(((MenuWrapper)ListBoxLab.SelectedItem).Control);
                ListBoxDevice.SelectedIndex = -1;
                ListBoxMaterial.SelectedIndex = -1;
            }
            
        }

        private void ListBoxDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListBoxDevice.SelectedIndex != -1)
            {
                PanelContent.Children.Clear();
                PanelContent.Children.Add(((MenuWrapper)ListBoxDevice.SelectedItem).Control);
                ListBoxLab.SelectedIndex = -1;
                ListBoxMaterial.SelectedIndex = -1;
            }

        }

        private void ListBoxMaterial_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListBoxMaterial.SelectedIndex != -1)
            {
                PanelContent.Children.Clear();
                // PanelContent.Content = ListBoxMaterial.SelectedItem;
                PanelContent.Children.Add(((MenuWrapper)ListBoxMaterial.SelectedItem).Control);
                ListBoxLab.SelectedIndex = -1;
                ListBoxDevice.SelectedIndex = -1;
            }
        }
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);
        }
    }
}
