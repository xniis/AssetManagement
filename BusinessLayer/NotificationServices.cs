﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EntityLayer;

namespace BusinessLayer
{
    public class NotificationServices
    {
        static public List<NotificationEntity> GetAllNotifications()
        {

            return NotificationDAO.GetAllNotifications();
        }

        static public NotificationEntity GetNotificationByID(int id)
        {
            return NotificationDAO.GetNotificationByID(id);
        }

        static public void Add(NotificationEntity newD)
        {
            NotificationDAO.AddNotification(newD);
        }

        static public void DeleteById(int id)
        {
            NotificationDAO.DeleteNotificationById(id);
        }

        static public void UpdateNotification(NotificationEntity updateD)
        {
            NotificationDAO.UpdateNotification(updateD);
        }
    }
}
