﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EntityLayer;

namespace BusinessLayer
{
    public class MaterialServices
    {
        static public List<MaterialEntity> GetAllMaterials()
        {
       
            return MaterialDAO.GetAllMaterialsList();
        }

        static public MaterialEntity GetMaterialByID(string id)
        {
            return MaterialDAO.GetMaterialByID(id);
        }

        static public void Add(MaterialEntity newD)
        {
            MaterialDAO.AddMaterial(newD);
        }

        static public void DeleteById(string id)
        {
            MaterialDAO.DeleteMaterialById(id);
        }

        static public void UpdateMaterial(MaterialEntity updateD)
        {
            MaterialDAO.UpdateMaterial(updateD);
        }
    }
}
