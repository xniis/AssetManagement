﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EntityLayer;

namespace BusinessLayer
{
    public class AccountServices
    {
        static public List<AccountEntity> GetAllAccounts()
        {
            return AccountDAO.GetAllAccountsList();
        }

        static public List<AccountEntity> GetAccountByName(string userName)
        {
            return AccountDAO.GetAccountByName(userName);
        }

        static public void AddUpdateAccount(AccountEntity newAccount)
        {
            AccountDAO.AddUpdateAccount(newAccount);
        }

        static public void DelUpdateAccount(AccountEntity delAccount)
        {
            AccountDAO.DeleteUpdateAccount(delAccount);
        }

        static public void UpdateAccount(AccountEntity updateAccount)
        {
            AccountDAO.UpdateAccount(updateAccount);
        }
    }
}
