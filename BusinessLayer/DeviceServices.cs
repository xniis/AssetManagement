﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EntityLayer;

namespace BusinessLayer
{
    public class DeviceServices
    {
        static public List<DeviceEntity> GetAllDevices()
        {
            return DeviceDAO.GetAllDevicesList();
        }

        static public DeviceEntity GetDeviceByID(string id)
        {
            return DeviceDAO.GetDeivceByID(id);
        }

        static public void Add(DeviceEntity newD)
        {
            DeviceDAO.AddDevice(newD);
        }

        static public void AddList(List<DeviceEntity> newList)
        {
            DeviceDAO.AddDevices(newList);
        }

        static public void DeleteById(string id)
        {
            DeviceDAO.DeleteDeviceById(id);
        }

        static public void UpdateDevice(DeviceEntity updateD)
        {
            DeviceDAO.UpdateDevice(updateD);
        }
    }
}
