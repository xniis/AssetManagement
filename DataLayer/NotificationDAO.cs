﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;

namespace DataLayer
{
    public class NotificationDAO
    {
        public static notification ToDBNotification(NotificationEntity a)
        {
            return new notification()
            {
                notification_subject = a.Subject,
                notification_time = a.Time           
            };
        }

        public static NotificationEntity ToNotificationEntity(notification n)
        {
            return new NotificationEntity
            {
                Id = n.notification_id,
                 Subject = n.notification_subject,

            };
        }

        public static List<NotificationEntity> GetAllNotifications()
        {
            List<NotificationEntity> accounts = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                accounts = (from a in db.notifications
                            select new NotificationEntity
                            {
                                Subject = a.notification_subject,
                                Time = a.notification_time
                            }
                            ).ToList();
            }
            return accounts;
        }

        public static void AddNotification(NotificationEntity newNotification)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                db.notifications.Add(ToDBNotification(newNotification));
                db.SaveChanges();
            }
        }

        public static void UpdateNotification(NotificationEntity update)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                notification old = (from a in db.notifications
                                           where a.notification_id == update.Id
                                           select a).First();
                old.notification_subject = update.Subject;
                old.notification_time = update.Time;
                db.SaveChanges();
            }
        }
        public static NotificationEntity GetNotificationByID(int id)
        {
            NotificationEntity d = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                d = (from a in db.notifications
                     where (a.notification_id == id)
                     select new NotificationEntity
                     {
                         Id = a.notification_id,
                         Subject = a.notification_subject,

                     }
                    ).FirstOrDefault();
            }
            return d;
        }

        public static void DeleteNotificationById(int id)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                notification u = db.notifications.Where(t => t.notification_id == id).FirstOrDefault();
                if (u != default(notification))
                {
                    db.notifications.Remove(u);
                }
                db.SaveChanges();
            }
        }
    }
}
