//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class lab_equipment
    {
        public string lab_equipment_number { get; set; }
        public string lab_equipment_name { get; set; }
        public int lab_equipment_amount { get; set; }
        public string lab_number { get; set; }
        public string lab_equipment_state { get; set; }
    }
}
