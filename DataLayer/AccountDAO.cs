﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;

namespace DataLayer
{
    public class AccountDAO
    {
        public static administrator ToDBAccount(AccountEntity a)
        {
            return new administrator()
            {
                admin_name = a.UserName,
                admin_number = a.UserId,
                password = a.UserPassword,
                privilege_level = a.UserLevel
            };
        }

        public static List<AccountEntity> GetAllAccountsList()
        {
            List<AccountEntity> accounts = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                accounts = (from a in db.administrators
                            select new AccountEntity
                            {
                                UserId = a.admin_number,
                                UserName = a.admin_name,
                                UserPassword = a.password,
                                UserLevel = a.privilege_level
                            }
                            ).ToList();
            }
            return accounts;
        }

        public static void AddUpdateAccount(AccountEntity a)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                db.administrators.Add(ToDBAccount(a));
                db.SaveChanges();
              
            }
        }

        public static void DeleteUpdateAccount(AccountEntity ae)
        {
            var a = ToDBAccount(ae);
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                administrator u = db.administrators.Where(t => t.admin_number == ae.UserId).FirstOrDefault();
                if(u.admin_name == a.admin_name
                && u.password == a.password
                && u.privilege_level == a.privilege_level
                && u.admin_number == a.admin_number)
                {
                    db.administrators.Remove(u);
                }
                db.SaveChanges();
            }
        }

        public static List<AccountEntity> GetAccountByName(string name)
        {
            List<AccountEntity> ae = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                ae = (from a in db.administrators where (a.admin_number == name) select new AccountEntity
                {
                    UserId = a.admin_number,
                    UserName = a.admin_name,
                    UserPassword = a.password,
                    UserLevel = a.privilege_level
                }).ToList();
            }
            return ae;
        }

        public static void UpdateAccount(AccountEntity ae)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                administrator aInDb = db.administrators.Where(t => t.admin_number == ae.UserId).FirstOrDefault();
                aInDb.password = ae.UserPassword;
                aInDb.privilege_level = ae.UserLevel;
                aInDb.admin_number = ae.UserId;
                db.SaveChanges();
            }
        }
    }
}
