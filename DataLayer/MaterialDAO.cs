﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;

namespace DataLayer
{
    public class MaterialDAO
    {
        public static inventory_equipment ToDBMaterial(MaterialEntity m)
        {
            return new inventory_equipment
            {
                inventory_amount = m.Amount,
                inventory_name = m.Name,
                inventory_number = m.Id,
                inventory_price = m.Price,
                experiment_equipment_state = m.State,
                storage_time = m.StorageTime
            };
        }

        public static MaterialEntity ToMaterialEntity(inventory_equipment i)
        {
            return new MaterialEntity
            {
                Amount=i.inventory_amount,
                Name= i.inventory_name,
                Id= i.inventory_number,
                Price= i.inventory_price,
                State= i.experiment_equipment_state,
                StorageTime= i.storage_time
            };
        }

        public static List<MaterialEntity> GetAllMaterialsList()
        {
            List<MaterialEntity> accounts = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                accounts = (from a in db.inventory_equipment
                            select new MaterialEntity
                            {
                                Amount = a.inventory_amount,
                                Name = a.inventory_name,
                                Id = a.inventory_number,
                                Price = a.inventory_price,
                                State = a.experiment_equipment_state,
                                StorageTime = a.storage_time
                            }
                            ).ToList();
            }
            return accounts;
        }

        public static void AddMaterial(MaterialEntity newMaterial)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                db.inventory_equipment.Add(ToDBMaterial(newMaterial));
                db.SaveChanges();
            }
        }

        public static void UpdateMaterial(MaterialEntity update)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                inventory_equipment old = (from a in db.inventory_equipment
                                     where a.inventory_number == update.Id
                                     select a).First();
                old.inventory_name = update.Name;
                old.inventory_amount = update.Amount;
                old.experiment_equipment_state = update.State;
                old.storage_time = update.StorageTime;
                old.inventory_price = update.Price;
                db.SaveChanges();
            }
        }
        public static MaterialEntity GetMaterialByID(string id)
        {
            MaterialEntity d = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                d = (from a in db.inventory_equipment
                     where (a.inventory_number == id)
                     select new MaterialEntity
                     {
                         Amount = a.inventory_amount,
                         Name = a.inventory_name,
                         Id = a.inventory_number,
                         Price = a.inventory_price,
                         State = a.experiment_equipment_state,
                         StorageTime = a.storage_time
                     }
                    ).FirstOrDefault();
            }
            return d;
        }

        public static void DeleteMaterialById(string id)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                inventory_equipment u = db.inventory_equipment.Where(t => t.inventory_number == id).FirstOrDefault();
                if (u != default(inventory_equipment))
                {
                    db.inventory_equipment.Remove(u);
                }
                db.SaveChanges();
            }
        }
    }
}
