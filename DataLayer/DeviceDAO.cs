﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityLayer;

namespace DataLayer
{
    public class DeviceDAO
    {
        public static lab_equipment ToDBLabEquipment(DeviceEntity de)
        {
            return new lab_equipment
            {
                lab_equipment_amount = de.Amount,
                lab_equipment_name = de.Name,
                lab_equipment_number = de.Id,
                lab_equipment_state = de.State,
                lab_number = de.LabId
            };
        }

        public static DeviceEntity ToDeviceEntity(lab_equipment l)
        {
            return new DeviceEntity
            {
                Amount = l.lab_equipment_amount,
                Name = l.lab_equipment_name,
                Id = l.lab_equipment_number,
                State = l.lab_equipment_state,
                LabId = l.lab_number
            };
        }

        public static List<DeviceEntity> GetAllDevicesList()
        {
            List<DeviceEntity> accounts = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                accounts = (from a in db.lab_equipment
                            select new DeviceEntity
                            {
                                Amount = a.lab_equipment_amount,
                                Name = a.lab_equipment_name,
                                Id = a.lab_equipment_number,
                                State = a.lab_equipment_state,
                                LabId = a.lab_number
                            } 
                            ).ToList();
            }
            return accounts;
        }

        public static void AddDevice(DeviceEntity newDevice)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                db.lab_equipment.Add(ToDBLabEquipment(newDevice));
                db.SaveChanges();
            }
        }

        public static void AddDevices(List<DeviceEntity> newDevice)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                foreach(var newD in newDevice)
                {
                    var n = (from a in db.lab_equipment
                             where a.lab_equipment_number == newD.Id
                             select a).FirstOrDefault();
                    if(n == default(lab_equipment))
                    {
                        db.lab_equipment.Add(ToDBLabEquipment(newD));
                    }
                }
                db.SaveChanges();
            }
        }
        public static void UpdateDevice(DeviceEntity update)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                lab_equipment old = (from a in db.lab_equipment
                                    where a.lab_equipment_number == update.Id
                                    select a).First();
                old.lab_equipment_name = update.Name;
                old.lab_equipment_amount = update.Amount;
                old.lab_equipment_state = update.State;
                db.SaveChanges();
            }
        }
        public static DeviceEntity GetDeivceByID(string id)
        {
            DeviceEntity d = null;
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                d = (from a in db.lab_equipment
                      where (a.lab_equipment_number == id)
                      select new DeviceEntity
                      {
                          Amount = a.lab_equipment_amount,
                          Name = a.lab_equipment_name,
                          Id = a.lab_equipment_number,
                          State = a.lab_equipment_state,
                          LabId = a.lab_number
                      }
                    ).FirstOrDefault();
            }
            return d;
        }

        public static void DeleteDeviceById(string id)
        {
            using (LaboratoryEntities db = new LaboratoryEntities())
            {
                lab_equipment u = db.lab_equipment.Where(t => t.lab_equipment_number == id).FirstOrDefault();
                if (u != default(lab_equipment))
                {
                    db.lab_equipment.Remove(u);
                }
                db.SaveChanges();
            }
        }

    }
}
